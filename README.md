# cfget.py

A tool for downloading Minecraft mods from CurseForge from the command line

### Installation

* Install dependencies:
    * Python 3
    * Selenium
    * Gecko-Webdriver
    * Firefox
* Copy cfget.py into your Minecraft mod folder
* Mark cfget.py as executable

### Usage

See `cfget.py --help`
