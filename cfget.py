#! /usr/bin/env python

import sys
import os
import time
import threading

# Import selenium
try:
    from selenium.webdriver import Firefox
    from selenium.webdriver.firefox.options import Options
    from selenium.webdriver import FirefoxProfile
    from selenium.common import exceptions as selenium_exceptions
except:
    print("ERR : Could not import selenium")
    print("      Did you install it?")
    exit()

from html.parser import HTMLParser

elems = []

GAME = "1.12.2"


# HTML parser
# It's a mess I know
class ElementFinder(HTMLParser):
    def __init__(self):
        super(ElementFinder, self).__init__()
        self.in_tr = False

    def handle_starttag(self, tag, attrs):
        # Handle opening tags
        # If encountered tag is <tr>, add a new element blob
        if tag == "tr" and self.in_tr == False:
            elems.append("")
            # Toggle bool on whether parser is inside a table row
            self.in_tr = True

        # If tag is a link and parser is inside a table row
        if tag == "a" and self.in_tr:
            for a in attrs:
                # If link is the mod download link, add it to element blob
                if a[0] == "href" and "/download/" in a[1] and "?client=y" not in a[1]:
                    # Elements separated by |
                    # Don't ask why
                    elems[-1] += a[1]+"|"

    def handle_endtag(self, tag):
        # Close element blob if table row ends
        if tag == "tr" and self.in_tr:
            elems[-1] = elems[-1][:-1]
            self.in_tr = False

    def handle_data(self, data):
        # Handle data encountered (tag texts)
        if self.in_tr:
            if data.strip():
                elems[-1] += data.strip()+"|"


# Set Firefox to headless mode
ff_opt = Options()
ff_opt.headless = True

# Create custom Firefox profile for automatic saving
ff_p = FirefoxProfile()
ff_p.set_preference("browser.download.panel.shown", False)
ff_p.set_preference("browser.download.folderList", 2)
ff_p.set_preference("browser.download.manager.showWhenStarting", False)
ff_p.set_preference("browser.download.dir", os.getcwd())
ff_p.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/java-archive,application/x-amz-json-1.0")

# Launch headless Firefox instance with Selenium
try:
    driver = Firefox(options=ff_opt, firefox_profile=ff_p)
except selenium_exceptions.WebDriverException:
    print("ERR : Could not load webdriver")
    print("      Did you install gecko-webdriver?")
    exit()

# CurseForge URL parts
CF_BASE_URL = "https://www.curseforge.com/minecraft/mc-mods/"
CF_FILE_URL = "/files/all"

# HTML parser instance
PARSER = ElementFinder()


# args from sys.argv
try:
    argv = sys.argv[1:]
except IndexError:
    argv = None

# Dict with all flags
all_flags = {
    ("-h", "--help")          : "help",
    ("-S", "-i", "--install") : "install",
    ("-g", "--game-version")  : "gamever"
}

args = {}

all_flags_raw = []
for f in all_flags.keys():
    all_flags_raw += f

# Create arg dictionary
if argv:
    for a in argv:
        if a in all_flags_raw:
            for f in all_flags.keys():
                if a in f:
                    last_f = all_flags[f]
                    args[last_f] = []
                    break
        else:
            args[last_f].append(a)

# Wait until download is complete
def dlWatch(filename):
    wait = True
    while wait:
        time.sleep(2)
        wait = False
        files = os.listdir(os.getcwd())

        for f in files:
            if f.startswith(filename):
                if f.endswith('.part'):
                    wait = True
    print(f"{filename} : Done")
    time.sleep(1)

# Gets file name and link
def getFile(mod, page=1):
    global elems
    elems = []
    # Retrieve mod files page (sorted by latest)
    print(f"Getting file list (Page {page})...")
    print(f"Accessing "+CF_BASE_URL+mod+CF_FILE_URL+"?page="+str(page))
    driver.get(CF_BASE_URL+mod+CF_FILE_URL+"?page="+str(page))

    # Get HTML source of file list page
    html = driver.page_source

    # Send source to HTML parser
    print("Parsing file list... ")
    PARSER.feed(html)
    # Remove last (empty) element from returned list
    elems = elems[1:]

    # Create 2D list from elements
    table = []
    for row in elems:
        table.append([])
        for col in row.split("|"):
            table[-1].append(col)

    file_name = None
    # Loop through 2D list and find latest mod version
    for row in table:
        if row[2] == "+1 More":
            del row[2]
        if row[4] == GAME:
            print(f"Found latest version for {GAME} : {row[1]}")
            file_name = row[1]
            file_link = "https://www.curseforge.com"+row[-1]+"/file"
            break

    if not file_name:
        return getFile(mod, page=page+1)
    else:
        return file_name, file_link


# Nondefault game version
if "gamever" in args.keys():
    GAME = args["gamever"][0]

# Help command
if not args or "help" in args.keys():
    print("USAGE:\n    cfget.py <FLAGS [ARGS]>...\n")
    print("FLAGS:")
    print("    -S, -i, --install   Download mods (ARGS) to current directory")
    print("    -h, --help          Show this help and exit")
    print("    -g, --game-version  Set game version (default 1.12.2)")
    exit()

# Installation command
if "install" in args.keys():
    if not args["install"]:
        print("[ERR] INSTALL : No mods specified")
        exit()
    dl_waiters = []
    # Loop through provided args (mod names)
    for mod in args["install"]:
        print(f"Installing {mod}")
        print(f"Looking for latest version for {GAME}")
        file_name, file_link = getFile(mod)

        # Download mod
        print(f"Saving to {os.getcwd()}/{file_name}")
        print(f"Downloading {file_link}...")

        # Hack to ignore the download page as we only care about the file
        driver.set_page_load_timeout(1)
        try:
            driver.get(file_link)
        except selenium_exceptions.TimeoutException:
            pass
        driver.set_page_load_timeout(30)
        # Add download waiter thread to list
        dl_waiters.append(threading.Thread(target=lambda: dlWatch(file_name)))
        dl_waiters[-1].start()

    # Close download threads and quit
    for t in dl_waiters:
        print("Waiting for remaining downloads...")
        t.join()
    print("Closing selenium")
    driver.quit()
